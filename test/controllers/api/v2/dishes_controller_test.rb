require 'test_helper'

class Api::V2::DishesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v2_dishes_index_url
    assert_response :success
  end

  test "should get show" do
    get api_v2_dishes_show_url
    assert_response :success
  end

end
