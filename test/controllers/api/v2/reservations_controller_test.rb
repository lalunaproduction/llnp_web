require 'test_helper'

class Api::V2::ReservationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v2_reservations_index_url
    assert_response :success
  end

end
