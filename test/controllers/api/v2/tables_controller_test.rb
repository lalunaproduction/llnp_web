require 'test_helper'

class Api::V2::TablesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v2_tables_index_url
    assert_response :success
  end

  test "should get show" do
    get api_v2_tables_show_url
    assert_response :success
  end

end
