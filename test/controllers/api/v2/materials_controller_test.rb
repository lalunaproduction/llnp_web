require 'test_helper'

class Api::V2::MaterialsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v2_materials_index_url
    assert_response :success
  end

end
