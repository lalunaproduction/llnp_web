require 'test_helper'

class Api::V2::OrderLocationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v2_order_locations_index_url
    assert_response :success
  end

end
