require 'test_helper'

class Admin::MaterialOrdersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get admin_material_orders_new_url
    assert_response :success
  end

  test "should get create" do
    get admin_material_orders_create_url
    assert_response :success
  end

  test "should get edit" do
    get admin_material_orders_edit_url
    assert_response :success
  end

  test "should get update" do
    get admin_material_orders_update_url
    assert_response :success
  end

  test "should get index" do
    get admin_material_orders_index_url
    assert_response :success
  end

  test "should get show" do
    get admin_material_orders_show_url
    assert_response :success
  end

  test "should get destroy" do
    get admin_material_orders_destroy_url
    assert_response :success
  end

end
