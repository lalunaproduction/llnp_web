namespace :deploy do
  desc 'run some rake db task'
  task :seeds do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute :rake, "db:seeds"
        end
      end
    end
  end
end