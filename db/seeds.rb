# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Role.create(name: 'Admin', description: 'Quyền quản trị viên', created_by: 'yle', updated_by: 'yle', delete_flag:false)
Role.create(name: 'Member', description: 'Thành viên', created_by: 'yle', updated_by: 'yle', delete_flag:false)
Role.create(name: 'Staff', description: 'Nhân Viên', created_by: 'yle', updated_by: 'yle', delete_flag:false)
Role.create(name: 'Partner', description: 'Người liên kết', created_by: 'yle', updated_by: 'yle', delete_flag:false)


User.create(name: 'yle', description: 'yle', password:'H0ngkhanH', role_id: 1, email: 'leminhy89@gmail.com', created_by: 'System', updated_by: 'System', delete_flag: false)
User.create(name: 'laluna', description: 'laluna', password:'12345', role_id: 1, email: 'lalunapalace@gmail.com', created_by: 'System', updated_by: 'System', delete_flag: false)
User.create(name: 'damasa', description: 'damasa', password:'12345', role_id: 2, email: 'damasa@gmail.com', created_by: 'System', updated_by: 'System', delete_flag: false)
User.create(name: 'danangtoday', description: 'danangtoday', password:'12345', role_id: 2, email: 'danangtoday@gmail.com', created_by: 'System', updated_by: 'System', delete_flag: false)

Language.create(name: 'vi', description: 'Tiếng Việt', created_by:'yle', updated_by:'yle', delete_flag: false)
Language.create(name: 'en', description: 'Tiếng Anh', created_by:'yle', updated_by:'yle', delete_flag: false)

ReservationStatus.create(name: 'Đặt Mới', description: 'Trạng thái khi đặt mới 1 bàn', created_by:'yle', updated_by:'yle', delete_flag: false)
ReservationStatus.create(name: 'Hủy', description: 'Trạng thái khi bàn đặt bị Hủy', created_by:'yle', updated_by:'yle', delete_flag: false)
ReservationStatus.create(name: 'Thành công', description: 'Trạng thái khi khách hàng đã đến và xác nhận', created_by:'yle', updated_by:'yle', delete_flag: false)
ReservationStatus.create(name: 'Không thành công', description: 'Trạng thái khi khách đặt bàn không đến', created_by:'yle', updated_by:'yle', delete_flag: false)

Category.create(name: 'Khách Sạn - Nhà Trọ', description: 'danh mục khách sạn, nhà nghỉ, nhà trọ', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)
Category.create(name: 'Nhà Hàng - Quán Ăn', description: 'danh mục nhà hàng, quán ăn, quán nhậu', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)
Category.create(name: 'Quán Bar - Pub - Cafe', description: 'danh mục Bar, pub, cafe', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)
Category.create(name: 'Danh Thắng', description: 'danh lam thắng cảnh', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)
Category.create(name: 'Mua Sắm', description: 'danh mục shoping, mua sắm', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)
Category.create(name: 'Spa', description: '	danh mục spa, làm đẹp, thư giản', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)
Category.create(name: 'Nhà Hàng', description: 'danh mục Nhà Hàng', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: 2)
Category.create(name: 'Đặt Trước', description: 'danh mục khách sạn, nhà nghỉ, nhà trọ', created_by:'yle', updated_by:'yle', delete_flag: false, parentCate: nil)

Location.create(user_id: 2, phone: '0905787337', latitude: '123', longitude: '123', created_by: 'yle', updated_by: 'yle', delete_flag: false)
LocationTranslation.create(location_id: 1, language_id: 1, name: "Laluna", description: "<p>LaLuna Restaurant</p>", address: "187 Huyền Trân Công Chúa - NHS - ĐN", created_by: "yle", updated_by: "yle", delete_flag: false)


