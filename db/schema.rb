# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180402072145) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "parentCate"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "connections", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "location_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "device_tokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "dishes", force: :cascade do |t|
    t.string   "name"
    t.integer  "price"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "languages", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "location_categories", force: :cascade do |t|
    t.integer  "location_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "location_translations", force: :cascade do |t|
    t.integer  "location_id"
    t.integer  "language_id"
    t.string   "name"
    t.string   "description"
    t.string   "address"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "locations", force: :cascade do |t|
    t.integer  "user_id",     default: 0
    t.string   "phone"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "material_orders", force: :cascade do |t|
    t.integer  "material_id"
    t.integer  "quantity"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.date     "order_date"
  end

  create_table "materials", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "unit"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "order_location_id"
  end

  create_table "order_dishes", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "dish_id"
    t.integer  "dish_price"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "order_locations", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "table_id"
    t.date     "order_date"
    t.integer  "pay_type"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "packages", force: :cascade do |t|
    t.string   "height"
    t.string   "width"
    t.string   "weight"
    t.string   "image"
    t.string   "note"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "pto_requests", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "date_off"
    t.string   "type"
    t.string   "reason"
    t.integer  "request_status_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "receivers", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "address"
    t.string   "district"
    t.string   "city"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "request_statuses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "reservation_statuses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "reservation_date"
    t.integer  "adult"
    t.integer  "child"
    t.integer  "reservation_status_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "hours"
    t.integer  "minutes"
    t.integer  "location_id"
  end

  create_table "role_requests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.integer  "request_status_id"
    t.string   "reason"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "from_role"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "delete_flag"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tables", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "user_receivers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "receiver_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.integer  "role_id"
    t.string   "description"
    t.string   "email"
    t.string   "encrypt_password"
    t.string   "salt"
    t.boolean  "delete_flag"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "token"
    t.integer  "customer_type_id"
  end

end
