class CreateMaterialOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :material_orders do |t|
      t.integer :material_id
      t.integer :quantity
      t.string :description
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
