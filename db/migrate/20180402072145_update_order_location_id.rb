class UpdateOrderLocationId < ActiveRecord::Migration[5.0]
  def change
    remove_column :material_orders, :order_location_id
    add_column :materials, :order_location_id, :integer
  end
end
