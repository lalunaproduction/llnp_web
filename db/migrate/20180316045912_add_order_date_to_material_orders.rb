class AddOrderDateToMaterialOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :material_orders, :order_date, :date
  end
end
