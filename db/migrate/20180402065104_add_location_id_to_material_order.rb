class AddLocationIdToMaterialOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :material_orders, :order_location_id, :integer
  end
end
