class CreateLocationTranslations < ActiveRecord::Migration[5.0]
  def change
    create_table :location_translations do |t|
      t.integer :location_id
      t.integer :language_id
      t.string :name
      t.string :description
      t.string :address
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
