class UpdateRoleRequest < ActiveRecord::Migration[5.0]
  def change
    remove_column :role_requests, :from_role_id
    add_column :role_requests, :from_role, :string
    rename_column :role_requests, :to_role_id, :role_id
  end
end
