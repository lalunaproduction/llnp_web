class CreateDishes < ActiveRecord::Migration[5.0]
  def change
    create_table :dishes do |t|
      t.string :name
      t.integer :price
      t.string :description
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
