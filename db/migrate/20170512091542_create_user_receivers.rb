class CreateUserReceivers < ActiveRecord::Migration[5.0]
  def change
    create_table :user_receivers do |t|
      t.integer :user_id
      t.integer :receiver_id
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
