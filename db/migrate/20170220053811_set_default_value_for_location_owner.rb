class SetDefaultValueForLocationOwner < ActiveRecord::Migration[5.0]
  def change
    change_column :locations, :user_id, :integer, :default => 0
  end
end
