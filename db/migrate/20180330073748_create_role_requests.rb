class CreateRoleRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :role_requests do |t|
      t.integer :user_id
      t.integer :from_role_id
      t.integer :to_role_id
      t.integer :request_status_id
      t.string :reason
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
