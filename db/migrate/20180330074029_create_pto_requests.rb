class CreatePtoRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :pto_requests do |t|
      t.integer :user_id
      t.string :date_off
      t.string :type
      t.string :reason
      t.integer :request_status_id
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
