Rails.application.routes.draw do

  namespace :api do
    namespace :v2 do
      get 'order_locations/index'
    end
  end

  namespace :api do
    namespace :v2 do
      resources :sessions, :reservations, :device_tokens, :materials, :material_orders, :tables, :dishes, :users, :order_locations
    end
  end

  #mount Ckeditor::Engine => '/ckeditor'
  mount API::Base => '/'
  mount GrapeSwaggerRails::Engine => '/docs'
  
  scope ":locale", locale: /#{I18n.available_locales.join("|")}/ do 
    
    root 'home#index'
    
    resources :home, :sessions, :privacy
    
    get '/dang-xuat' => 'sessions#destroy', :as => 'dang-xuat'
    get '/dang-nhap' => 'sessions#new', :as => 'dang-nhap'
    
    namespace :admin do
      resources :dashboard, :roles, :users, :reservation_statuses, :languages, :categories, :materials, 
      :material_orders, :tables, :dishes, :request_statuses, :role_requests, :order_locations
      
      resources :locations do 
        collection do
          get '/:id/assignOwner' => :assignOwnerView, as: "assignOwner"
          post '/:id/assignOwner' => :assignOwner, as: "assignOwnerP"
        end
      end
      
      resources :reservations do
        collection do
          get 'getListCurrentData'
          post '/:id/cancelReservation' => :cancelReservation, as: "cancelReservation"
          post '/:id/confirmReservationOK' => :confirmReservationOK, as: "confirmReservationOK"
          post '/:id/confirmReservationNoK' => :confirmReservationNoK, as: "confirmReservationNoK"
        end
      end
      
      resources :restaurants do
        collection do
          get '/:id/makeConnection' => :makeConnection, as: "makeConnection"
        end
      end
    end 
    
    
    
  end
  
  get '*path', to: redirect("/#{I18n.default_locale}/%{path}")
  get '', to: redirect("/#{I18n.default_locale}")
  #get 'reservations/getListCurrentData', to: 

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  # resources :home
  
  # namespace :admin do
  #   resources :dashboard, :roles
  # end
  
  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
