class Location < ApplicationRecord
    attr_accessor :name, :description, :language_id, :address
    
    has_many :location_translations
    belongs_to :language
    belongs_to :user
    has_many :location_categories, dependent: :destroy
    has_many :categories, through: :location_categories
    has_many :reservations
    has_many :connections
    
end
