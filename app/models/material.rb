class Material < ApplicationRecord
    has_many :material_orders
    belongs_to :order_location
end
