class Role < ApplicationRecord
    has_many :users
    has_many :role_requests
end
