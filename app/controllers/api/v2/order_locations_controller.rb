class Api::V2::OrderLocationsController < Api::V2::ApplicationController
  before_action :verify_authorzation
  skip_before_filter :verify_authenticity_token, :only => :create
  
  def index
    @orderLocations = OrderLocation.where(delete_flag: 0)

    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Khu Vực",
           "result": @orderLocations.as_json(except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by])
     }
  end
end
