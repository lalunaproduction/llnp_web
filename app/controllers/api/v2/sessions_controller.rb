class Api::V2::SessionsController < Api::V2::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => :create
  
  def create
    information = request.raw_post
    data_parsed = JSON.parse(information)
    user = User.authenticate(data_parsed["name"], data_parsed["password"])
    
    if user.nil?
      render json: {
         "status": :create,
         "code": 401,
         "messages":"Đăng nhập thất bại",
         "result": ""
        }
    else
      render json: {
        "status": :create,
         "code": 200,
         "messages":"Đăng nhập Thành Công",
         "result": user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
          include: {
            role: {
              except: [:delete_flag]
            }
         })
      } 
    end
  end

  def destroy
  end
  
  private
    def login_params
      params.permit(:name, :password)
    end
  
end
