class Api::V2::TablesController < Api::V2::ApplicationController
  def index
    @tables = Table.where(delete_flag: 0)
    
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Bàn",
           "result": @tables.as_json(except: [:delete_flag])
     }
  end

  def show
    @table = Table.find(params[:id])
    
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Chi Tiết Bàn",
           "result": @table.as_json(except: [:delete_flag])
     }
  end
end
