class Api::V2::MaterialordersController < Api::V2::ApplicationController
  before_action :verify_authorzation
  skip_before_filter :verify_authenticity_token, :only => :create
  
  def index
    if !params[:date].nil?
        @materials = MaterialOrder.where(delete_flag: 0, order_date: params[:date]) 
    else
      @materials = MaterialOrder.where(delete_flag: 0)
    end
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Đơn Hàng",
           "result": @materials.as_json(except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by],
           include: {
            material: {
              except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
            }
         })
     }
     
  end
  
  def create
      
  end
end
