class Api::V2::MaterialOrdersController < Api::V2::ApplicationController
  before_action :verify_authorzation
  skip_before_filter :verify_authenticity_token, :only => [:create, :destroy, :update]
  
  def new
  end

  def create
    @material_order = MaterialOrder.new
    @material_order.material_id = params[:material_id]
    @material_order.description = params[:description]
    @material_order.quantity = params[:quantity]
    @material_order.order_date = params[:order_date]
    @material_order.created_by = @user.name
    @material_order.updated_by = @user.name
    @material_order.delete_flag = 0
   
   
    unless isOrderExistedOnThisDay?(params[:material_id], params[:order_date])
    
      if @material_order.save
        render json: {
              "status": "OK",
              "code": "200",
              "messages":"Đơn Hàng Tạo Thành Công",
              "result": @material_order.as_json(except: [:delete_flag])
            }
      else
        render json: {
              "status": "NoK",
              "code": "201",
              "messages":"Đơn Hàng Tạo Thất Bại",
              "result": ""
            }
      end 
    else 
      render json: {
              "status": "NoK",
              "code": "201",
              "messages":"Đơn Hàng đã tồn tại",
              "result": ""
            }
    end
    
  end

  def index
    if !params[:date].nil?
        @materials = MaterialOrder.where(delete_flag: 0, order_date: params[:date]) 
    else
      @materials = MaterialOrder.where(delete_flag: 0)
    end
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Đơn Hàng",
           "result": @materials.as_json(except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by],
           include: {
            material: {
              except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
            }
         })
     }
     
  end

  def show
    return render json: {
           "status": "OK",
           "code": "200",
           "messages":"Cập nhật Đơn Hàng",
           "result": MaterialOrder.find(params[:id]).as_json(except: [:delete_flag],
           include: {
             material: {
               except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
             }
           })
        }
  end
  
  def update
    
    information = request.raw_post
    data_parsed = JSON.parse(information)
    
    #check normal update
    if data_parsed["force_update"] == "false"
      #this is normal update
      #validate existed item
      @existedItem = isOrderExisted params[:id], data_parsed["material_id"], data_parsed["order_date"]
      if @existedItem.nil?
        
        #the update item is not existed
        @materialOrder = MaterialOrder.find(params[:id])
        @materialOrder.material_id = data_parsed["material_id"]
        @materialOrder.description = data_parsed["description"]
        @materialOrder.quantity = data_parsed["quantity"]
        @materialOrder.order_date = data_parsed["order_date"]
        @materialOrder.updated_by = @user.name
        
        if @materialOrder.save
          render json: {
                "status": "OK",
                "code": "200",
                "messages":"Cập nhật Đơn Hàng thành công",
                "result": @materialOrder.as_json(except: [:delete_flag],
                include: {
                  material: {
                   except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
                 }
                })
              }
        else
          render json: {
                "status": "NoK",
                "code": "201",
                "messages":"Cập nhật Đơn Hàng Thất Bại",
                "result": ""
              }
        end
      else
        #the updated item is exited
        render json: {
                "status": "NoK",
                "code": "202",
                "messages":"Đơn Hàng đã tồn tại",
                "result": @existedItem.as_json(except: [:delete_flag],
                include: {
                  material: {
                    except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
                  }
                })
              }
      end
    else
      #force update and remove one
      @existed = MaterialOrder.find(data_parsed["existed_order_id"])
      @existed.delete_flag = 1
      if @existed.save
        #update item
        @materialOrder = MaterialOrder.find(params[:id])
        @materialOrder.material_id = data_parsed["material_id"]
        @materialOrder.description = data_parsed["description"]
        @materialOrder.quantity = data_parsed["quantity"]
        @materialOrder.order_date = data_parsed["order_date"]
        @materialOrder.updated_by = @user.name
        
        if @materialOrder.save
          render json: {
                "status": "OK",
                "code": "200",
                "messages":"Cập nhật Đơn Hàng thành công",
                "result": @materialOrder.as_json(except: [:delete_flag],
                include: {
                  material: {
                   except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
                 }
                })
              }
        else
          render json: {
                "status": "NoK",
                "code": "201",
                "messages":"Cập nhật Đơn Hàng Thất Bại",
                "result": ""
              }
        end
      else
        render json: {
                "status": "NoK",
                "code": "201",
                "messages":"Cập nhật đơn hàng thất bại",
                "result": @existed.as_json(except: [:delete_flag],
                include: {
                  material: {
                    except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
                  }
                })
              } 
      end
    end
  end
  
  def destroy 
    @materialOrder = MaterialOrder.find(params[:id])
    @materialOrder.delete_flag = 1
    
    if @materialOrder.save
      return render json: {
           "status": "OK",
           "code": "200",
           "messages":"Xoá Đơn Hàng Thành Công",
           "result": @materialOrder.as_json(
           include: {
             material: {
               except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
             }
           })
        }
    else
      return render json: {
           "status": "NOK",
           "code": "201",
           "messages":"Xoá Đơn Hàng Thất Bại",
           "result": ""
        }
    end
  end
  
  private
    def isOrderExisted (current_order_id, material_id, order_date)
      return MaterialOrder.where("material_id = ? AND order_date = ? AND id != ? AND delete_flag = false",material_id,order_date,current_order_id).first 
    end
    
    def isOrderExistedOnThisDay? (material_id, order_date)
      @orders = MaterialOrder.where("material_id = ? AND order_date = ? AND delete_flag = false",material_id,order_date)
      
      if @orders.length > 0
        true
      else
        false
      end
    end
end
