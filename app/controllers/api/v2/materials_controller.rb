class Api::V2::MaterialsController < Api::V2::ApplicationController
  before_action :verify_authorzation
  
  def index
    if params[:location].nil?
      @materials = Material.where(delete_flag: 0)
    else
      @materials = Material.where(order_location_id: params[:location], delete_flag: 0)
    end
    
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Hàng",
           "result": @materials.as_json(except: [:delete_flag],
           include: {
            order_location: {
              except: [:delete_flag, :created_at, :updated_at, :created_by, :updated_by]
            }
         })
     }
  end
end
