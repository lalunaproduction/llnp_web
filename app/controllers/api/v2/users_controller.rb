class Api::V2::UsersController < Api::V2::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:create]
  
  def create
    @user = User.new
    
    @user.name = params[:name].downcase
    @user.role_id = 2
    @user.description = "Register from Mobile Application"
    @user.email = params[:email].downcase
    @user.password = params[:password]
    @user.created_by = @user.name
    @user.updated_by = @user.name
    @user.delete_flag = 0
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    @user.token = (0...32).map { o[rand(o.length)] }.join
    
    unless isUserExist? @user.name, @user.email
      if @user.save
        render json: {
              "status": "OK",
              "code": "200",
              "messages":"User has been Created.",
              "result": @user.as_json(except: [:delete_flag],
              include: {
                role: {
                  except: [:delete_flag]
                }
              })
            }
      else
        render json: {
              "status": "NoK",
              "code": "201",
              "messages":"Register failed. Please try again later.",
              "result": ""
            }
      end
    else
      render json: {
              "status": "NoK",
              "code": "201",
              "messages":"User has been existed. Please try another one.",
              "result": ""
            }
    end
    
  end
  
  private
    def isUserExist? name, email
      @users = User.where(name: name).or(User.where(email: email))
      
      if @users.length > 0
        true
      else
        false
      end
    end
end
