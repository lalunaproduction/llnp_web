class Api::V2::DishesController < Api::V2::ApplicationController
  def index
    @dishes = Dish.where(delete_flag: 0)
    
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Món Ăn",
           "result": @dishes.as_json(except: [:delete_flag])
     }
  end

  def show
    @dish = Dish.find(params[:id])
    
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Chi Tiết Món Ăn",
           "result": @dish.as_json(except: [:delete_flag])
     }
  end
  
end
