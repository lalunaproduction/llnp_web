class Api::V2::ReservationsController < Api::V2::ApplicationController
  before_action :verify_authorzation
  skip_before_filter :verify_authenticity_token, :only => :create
  
  def index
    if !params[:date].nil?
      resDate = params[:date]+" 00:00:00"
      resDate = DateTime.parse(params[:date]+" 00:00:00").to_date
      resDate = (resDate-1).to_s + " 17:00:00"
      
      if @user.role.name == "Admin"
        @reservations = Reservation.where(delete_flag: 0, reservation_date: resDate).order(hours: :asc, minutes: :asc)  
      else
        @reservations = Reservation.where(delete_flag: 0, reservation_date: resDate, created_by: @user.name).order(hours: :asc, minutes: :asc)  
      end
    else
      if @user.role.name == "Admin"
        @reservations = Reservation.where(delete_flag: 0).order(hours: :asc, minutes: :asc)
      else
        @reservations = Reservation.where(delete_flag: 0, created_by: @user.name).order(hours: :asc, minutes: :asc)
      end
    end
    
     render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Đơn Hàng",
           "result": @reservations.as_json(except: [:delete_flag])
     }
    
  end
  
  def show
    @reservation = Reservation.find(params[:id])
    
    if params[:code].nil?
      render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Đơn Hàng",
           "result": @reservation.as_json(except: [:delete_flag])
      }
    else
      if params[:code] == "OK"
        @reservation.reservation_status_id = 3
      elsif params[:code] == "NoK"
        @reservation.reservation_status_id = 4
      elsif params[:code] == "Cancel"
        @reservation.reservation_status_id = 2
      end
      
      @reservation.updated_at = DateTime.now
      @reservation.updated_by = @user.name
      
      if @reservation.save
        return render json: {
           "status": "OK",
           "code": "200",
           "messages":"Cập nhật Đơn Hàng",
           "result": Reservation.find(params[:id]).as_json(except: [:delete_flag])
        }
      else
        return render json: {
           "status": "OK",
           "code": "201",
           "messages":"Cập nhật Đơn Hàng Thất Bại",
           "result": ""
        }
      end
    end
  end
  
  def create
    @reservation = Reservation.new
    @reservation.created_by = @user.name
    @reservation.updated_by = @user.name
    @reservation.delete_flag = 0
    @reservation.reservation_status_id = 1
    
    @reservation.location_id = LocationTranslation.find(1).location_id
    @reservation.name = params[:name]
    @reservation.description = params[:description]
    @reservation.reservation_date = params[:reservation_date]
    @reservation.hours = params[:hours]
    @reservation.minutes = params[:minutes]
    @reservation.adult = params[:adult]
    @reservation.child = params[:child]
    
    if isTimeValid? @reservation
      unless isExistRes? @reservation
        if @reservation.save
          
          #send notification ro administrator
          @listAdmin = User.where(role_id: 1, delete_flag: 0).select(:id)
          
          @listToken = DeviceToken.where(user_id: @listAdmin, delete_flag: 0)
          
          @FCM_KEY_API  = "AAAAkuqoNl4:APA91bGQQu03JWfnPHjGpsLkVR-0qZt5ZwF-KDj2cxQNjztXpRG7jiqZN77qa4rZruoiJbgtjwvWPxda_USgKYMbHJtOFCL-qs1BYUGV8MhEORn1u5BhHpp2MVYNPzFN5tf8Bi99h15PZQhgEppBn5HgIhGTOZodYg"
          @FCM_SEND_API = "https://fcm.googleapis.com/fcm/send"
          
          registration_ids = []
          @listToken.each do |f|
            registration_ids.push(f.token)
          end
          
          body = {
            "registration_ids" => registration_ids,
            "notification" => {
              "title": "Đơn đặt bàn mới từ "+@reservation.created_by,
              "body":"Ngày đặt: "+@reservation.reservation_date.strftime("%d/%m/%Y")+"\n"+@reservation.adult.to_s+" Lớn, "+@reservation.child.to_s+" Nhỏ"+"\n"+"Vào lúc: "+@reservation.hours.to_s+"h"+@reservation.minutes.to_s,
              "reservationDate": @reservation.reservation_date,
              "reservationFrom": @reservation.created_by,
              "adult": @reservation.adult,
              "child": @reservation.child,
              "sound": true
            }
          }
          
          HTTParty.post(@FCM_SEND_API,
          { 
            :body => body.to_json,
            :headers => { 'Content-Type' => 'application/json', 'Authorization' => 'key=AAAAkuqoNl4:APA91bGQQu03JWfnPHjGpsLkVR-0qZt5ZwF-KDj2cxQNjztXpRG7jiqZN77qa4rZruoiJbgtjwvWPxda_USgKYMbHJtOFCL-qs1BYUGV8MhEORn1u5BhHpp2MVYNPzFN5tf8Bi99h15PZQhgEppBn5HgIhGTOZodYg'}
          })
          
          render json: {
            "status": "OK",
            "code": "200",
            "messages":"Đơn Hàng Tạo Thành Công",
            "result": @reservation.as_json(except: [:delete_flag])
          }
          
        else
          render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Tạo lỗi."
          }
        end
      else
        render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Reservation is already existed."
        }
      end
    else
      render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Reservation date is not valid."
      }
    end
  end
  
  private
    def isExistRes? reservation
      @res = Reservation.where(delete_flag: 0,
                        name: reservation.name,
                        adult: reservation.adult,
                        child: reservation.child,
                        reservation_status_id: 1,
                        hours: reservation.hours,
                        minutes: reservation.minutes,
                        location_id: reservation.location_id,
                        reservation_date: reservation.reservation_date
                        )
      if @res.length > 0
        true
      else
        false
      end
    end
    
    def isTimeValid? reservation
      @resTime = reservation.reservation_date + (reservation.hours*60*60) + (reservation.minutes*60)
      
      @currTime = Time.zone.now
      @checkingTime = @currTime + (2*60*60)
      if @resTime >= @checkingTime
        true
      else
        false
      end
    end
  
end
