module API 
    module V1
        module Defaults
            extend ActiveSupport::Concern

            HEADERS_DOCS = {
                Authorization: {
                    description: "User Authorization Token",
                    required: true
                }
            }
            
            included do
                prefix "api"
                version "v1", using: :path
                default_format :json
                format :json
                formatter :json,
                    Grape::Formatter::ActiveModelSerializers
                    
                helpers do
                    def permitted_params
                        @permitted_params ||= declared(params, include_missing: false)
                    end
                    
                    def logger
                        Rails.logger
                    end
                    
                    def render_error(code, message, debug_info = '')
                        error!({meta: {code: code, message: message, debug_info: debug_info}}, code)
                    end
                
                    def render_success(json, extra_meta = {})
                        {data: json, meta: {code: RESPONSE_CODE[:success], message: "success"}.merge(extra_meta)}
                    end
                end
                
                rescue_from ActiveRecord::RecordNotFound do |e|
                    error_response(message: e.message, status: 404)
                end
                
                rescue_from ActiveRecord::RecordInvalid do |e|
                    error_response(message: e.message, status: 422)
                end
            end
        end
    end
end