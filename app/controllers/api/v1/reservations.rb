module API 
  module V1 
    class Reservations < Grape::API 
      include API::V1::Defaults
      
      resource :reservations do
        
        desc "Get all Reservations", headers: HEADERS_DOCS, http_codes: [
          { code: 200, message: 'success' },
          { code: RESPONSE_CODE[:not_found], message: "Không tìm thấy dữ liệu" }
        ]

        get "", root: :reservations do
          listReservation = Reservation.where(delete_flag: 0).all
          render_success(listReservation.as_json)
        end
        
        desc "Return a reservation", headers: HEADERS_DOCS, http_codes: [
          { code: 200, message: 'success' },
          { code: RESPONSE_CODE[:not_found], message: "Không tìm thấy dữ liệu" }
        ]
        params do 
          requires :id, type: String, desc: "ID of the reservation"
        end
        get ":id", root: "reservation" do
          res = Reservation.where(id: permitted_params[:id], delete_flag: 0).first!
          render_success(res.as_json)
        end
        
        
        desc "Return a reservation by date", headers: HEADERS_DOCS, http_codes: [
          { code: 200, message: 'success' },
          { code: RESPONSE_CODE[:not_found], message: "Không tìm thấy dữ liệu" }
        ]
        params do
          requires :date, type: String, desc: "Reservation date"
        end
        get "/byDate/:date", root: "reservation" do
          resDate = permitted_params[:date]+" 00:00:00"
          resDate = DateTime.parse(permitted_params[:date]+" 00:00:00").to_date
          resDate = (resDate-1).to_s + " 17:00:00"
          
          listReservation = Reservation.where(delete_flag: 0, reservation_date: resDate).all
          render_success(listReservation.as_json)
        end
        
        desc "Confirm the reservation is OK", headers: HEADERS_DOCS, http_codes: [
          { code: 200, message: "success" },
          { code: RESPONSE_CODE[:not_found], message: "no records" }
        ]
        params do
          requires :id, type: String, desc: "Reservation ID"
        end
        get "/:id/hasReservationOK", root: "reservation" do
          reservation = Reservation.find(params[:id])
    
          if reservation.update(:updated_at => DateTime.now, :updated_by => "laluna", :reservation_status_id => 3)
            render_success(reservation.as_json)
          end  
        end
        
        desc "Confirm the reservation is NoK", headers: HEADERS_DOCS, http_codes: [
          { code: 200, message: "success" },
          { code: RESPONSE_CODE[:not_found], message: "no records" }
        ]
        params do
          requires :id, type: String, desc: "Reservation ID"
        end
        get "/:id/hasReservationNoK", root: "reservation" do
          reservation = Reservation.find(params[:id])
    
          if reservation.update(:updated_at => DateTime.now, :updated_by => "laluna", :reservation_status_id => 4)
            render_success(reservation.as_json)
          end  
        end
        
        desc "Cancel the reservation", headers: HEADERS_DOCS, http_codes: [
          { code: 200, message: "success" },
          { code: RESPONSE_CODE[:not_found], message: "no records" }
        ]
        params do
          requires :id, type: String, desc: "Reservation ID"
        end
        get "/:id/cancel", root: "reservation" do
          reservation = Reservation.find(params[:id])
    
          if reservation.update(:updated_at => DateTime.now, :updated_by => "damasa", :reservation_status_id => 2)
            render_success(reservation.as_json)
          end  
        end
      end
    end
  end
end