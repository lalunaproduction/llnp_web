class Admin::MaterialsController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @material = Material.new
  end

  def create
    @material = Material.new(material_params)
    @material.created_by = current_user.name
    @material.updated_by = current_user.name
    @material.delete_flag = 0
    
    if @material.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_materials_path
    else
      render 'new'
    end
  end

  def index
    @materials = Material.where(delete_flag: 0).all
  end

  def show
  end

  def edit
    @material = Material.find(params[:id])
  end

  def update
    @material = Material.find(params[:id])
    @material.name = params[:material][:name]
    @material.order_location_id = params[:material][:order_location_id]
    @material.description = params[:material][:description]
    @material.description = params[:material][:unit]
    @material.updated_by = current_user.name
    
    if @material.save
      flash[:success] = 'Cập nhật thành công'
      redirect_to admin_materials_path
    else
      render 'edit'
    end
  end

  def destroy
  end
  
  private
    def material_params
      params.require(:material).permit(:name, :order_location_id, :description, :unit, :created_by, :updated_by, :delete_flag)
    end
end
