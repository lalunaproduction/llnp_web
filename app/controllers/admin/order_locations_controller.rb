class Admin::OrderLocationsController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @orderLocation = OrderLocation.new
  end

  def create
    @orderLocation = OrderLocation.new(location_params)
    @orderLocation.created_by = "Admin"
    @orderLocation.updated_by = "Admin"
    @orderLocation.delete_flag = 0
    
    if @orderLocation.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_order_locations_path
    else
      render 'new'
    end
  end

  def index
    @orderLocations = OrderLocation.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def location_params
      params.require(:order_location).permit(:name, :description, :created_by, :updated_by, :delete_flag)
    end
end
