class Admin::TablesController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @table = Table.new
  end

  def create
    @table = Table.new(table_params)
    @table.created_by = current_user.name
    @table.updated_by = current_user.name
    @table.delete_flag = 0
    
    if @table.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_tables_path
    else
      render 'new'
    end
  end

  def index
    @tables = Table.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def table_params
      params.require(:table).permit(:name, :description, :created_by, :updated_by, :delete_flag)
    end
end
