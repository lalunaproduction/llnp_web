class Admin::UsersController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.created_by = "Admin"
    @user.updated_by = "Admin"
    @user.delete_flag = 0
    #generate Token
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    @user.token = (0...32).map { o[rand(o.length)] }.join
    
    if @user.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_users_path
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = user.find(params[:id])
    @user.role_id = params[:role_id]
    @user.name = params[:name]
    @user.description = params[:description]
    
    if @user.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_users_path
    else
      render 'edit'
    end
  end

  def index
    @users = User.where(delete_flag: 0).all
  end

  def show
  end

  def destroy
  end
  
  private
    def user_params
      params.require(:user).permit(:name, :role_id, :description, :email, :password)
    end
end
