class Admin::LocationsController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @location = Location.new
  end

  def create
    @location = Location.new(location_params)
    @location.created_by = "yle"
    @location.updated_by = "yle"
    @location.delete_flag = 0
    
    if @location.save
      @location_translation = LocationTranslation.new
      @location_translation.location_id = @location.id
      @location_translation.language_id = (params["location"]["language_id"]).to_i
      @location_translation.name = params["location"]["name"]
      @location_translation.description = params["location"]["description"]
      @location_translation.address = params["location"]["address"]
      @location_translation.created_by = @location.created_by
      @location_translation.created_at = @location.created_at
      @location_translation.updated_by = @location.updated_by
      @location_translation.updated_at = @location.updated_at
      @location_translation.delete_flag = 0
      
      if @location_translation.save
        redirect_to admin_locations_path
      else
        render 'new'
      end
    else
      render 'new'
    end
    
  end

  def edit
  end

  def update
  end

  def index
    @lang = Language.find_by("name": params[:locale])
    @locations = Location.joins(:location_translations).where(['location_translations.language_id = ?', @lang.id])
  end

  def show
  end

  def destroy
  end
  
  def assignOwnerView
    @location = Location.new
  end
  
  def assignOwner
    @location = Location.find(params[:id])
    
    if @location.update(user_id: params["location"]["user_id"].to_i) 
      redirect_to admin_locations_path
    else
      redirect_to admin_locations_path
    end
  end
  
  
  private
    def location_params
      params.require(:location).permit(
        :latitude, 
        :longitude, 
        :user_id, 
        :phone, 
        :created_by, 
        :updated_by, 
        :delete_flag,
        category_ids: [])
    end
end
