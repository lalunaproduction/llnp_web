class Admin::RequestStatusesController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @requestStatus = RequestStatus.new
  end

  def create
    @requestStatus = RequestStatus.new(request_status_params)
    @requestStatus.created_by = "Admin"
    @requestStatus.updated_by = "Admin"
    @requestStatus.delete_flag = 0
    
    if @requestStatus.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_request_statuses_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
  end

  def index
    @requestStatuses = RequestStatus.where(delete_flag: 0).all
  end

  def show
  end

  def destroy
  end
  
  private
    def request_status_params
      params.require(:request_status).permit(:name, :description, :created_by, :updated_by, :delete_flag)
    end
end
