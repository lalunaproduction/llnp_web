class Admin::RestaurantsController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def index
    @parentCate = Category.find_by("name":"Nhà Hàng - Quán Ăn")
    @categoryIds = Category.where("parentCate":@parentCate.id.to_s).or(Category.where(id: @parentCate.id)).select(:id)
    
    @lang = Language.find_by("name": params[:locale])
    @restaurants = Location.joins(:location_translations, :location_categories).where(['location_translations.language_id = ?', @lang.id]).where('location_categories.category_id':@categoryIds)
  end

  def show
  end

  def destroy
  end
  
  def makeConnection
    @user_id = current_user.id
    @location_id = params[:id]
    
    @connection = Connection.new
    @connection.user_id = current_user.id
    @connection.location_id = @location_id
    @connection.created_by = current_user.name
    @connection.updated_by = current_user.name
    @connection.delete_flag = 0
    
    if @connection.save
      redirect_to admin_restaurants_path
    else
      redirect_to admin_restaurants_path
    end
  end
  
  def isConnected(user_id, location_id)
    @ls = Connection.find_by("user_id":user_id, "location_id":location_id)
    if @ls.nil?
      false
    else
      true
    end
  end
  helper_method :isConnected
end
