class Admin::ReservationsController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @reservation = Reservation.new
  end

  def create
    @reservation = Reservation.new(reservation_params)
    @reservation.created_by = current_user.name
    @reservation.updated_by = current_user.name
    @reservation.delete_flag = 0
    @reservation.reservation_status_id = 1
    
    if isTimeValid? @reservation
      unless isExistRes? @reservation
        if @reservation.save
          
          #send notification ro administrator
          @listAdmin = User.where(role_id: 1, delete_flag: 0).select(:id)
          
          @listToken = DeviceToken.where(user_id: @listAdmin, delete_flag: 0)
          
          @FCM_KEY_API  = "AAAAkuqoNl4:APA91bGQQu03JWfnPHjGpsLkVR-0qZt5ZwF-KDj2cxQNjztXpRG7jiqZN77qa4rZruoiJbgtjwvWPxda_USgKYMbHJtOFCL-qs1BYUGV8MhEORn1u5BhHpp2MVYNPzFN5tf8Bi99h15PZQhgEppBn5HgIhGTOZodYg"
          @FCM_SEND_API = "https://fcm.googleapis.com/fcm/send"
          
          registration_ids = []
          @listToken.each do |f|
            registration_ids.push(f.token)
          end
          
          body = {
            "registration_ids" => registration_ids,
            "notification" => {
              "title": "Đơn đặt bàn mới từ "+@reservation.created_by,
              "body":"Ngày đặt: "+@reservation.reservation_date.strftime("%d/%m/%Y")+"\n"+@reservation.adult.to_s+" Lớn, "+@reservation.child.to_s+" Nhỏ"+"\n"+"Vào lúc: "+@reservation.hours.to_s+"h"+@reservation.minutes.to_s,
              "reservationDate": @reservation.reservation_date,
              "reservationFrom": @reservation.created_by,
              "adult": @reservation.adult,
              "child": @reservation.child,
              "sound": true
            }
          }
          
          HTTParty.post(@FCM_SEND_API,
          { 
            :body => body.to_json,
            :headers => { 'Content-Type' => 'application/json', 'Authorization' => 'key=AAAAkuqoNl4:APA91bGQQu03JWfnPHjGpsLkVR-0qZt5ZwF-KDj2cxQNjztXpRG7jiqZN77qa4rZruoiJbgtjwvWPxda_USgKYMbHJtOFCL-qs1BYUGV8MhEORn1u5BhHpp2MVYNPzFN5tf8Bi99h15PZQhgEppBn5HgIhGTOZodYg'}
          })
          
          flash[:success] = t("laluna.messages.created_success")
          redirect_to admin_reservations_path
        else
          flash[:error] = t("laluna.messages.error_create")
          render 'new'
        end
      else
        flash[:error] = t("laluna.messages.reservation_existed")
        render 'new'
      end
    else
      flash[:error] = t("laluna.messages.reservation_timeNotValid")
      render 'new'
    end
  end

  def index
    if is_admin?
      @reservations = Reservation.where(delete_flag: 0)
                    .includes(:location)
                    .order(reservation_date: :desc)
                    .paginate(:page => params[:page], :per_page => 20)
                    
    else
      
      @listLocOwner = Location.where(user_id: current_user.id)
      #check if current user is owner
      if @listLocOwner.length > 0
        #get all data 
        @reservations = Reservation.where(delete_flag: 0, location_id: @listLocOwner)
                          .or(Reservation.where(delete_flag:0 , created_by: current_user.name))
                          .includes(:location)
                          .order(reservation_date: :desc)
                          .paginate(:page => params[:page], :per_page => 20)
      else
        @reservations = Reservation.where(delete_flag:0 , created_by: current_user.name)
                          .includes(:location)
                          .order(reservation_date: :desc)
                          .paginate(:page => params[:page], :per_page => 20)
      end
      
    end
  end

  def show
  end

  def edit
    @reservation = Reservation.includes(:location).find(params[:id])
  end

  def update
    @reservation = Reservation.find(params[:id])
    @reservation.updated_by = current_user.name
    
    if @reservation.update(reservation_params)
      redirect_to admin_reservations_path
    else
      render 'edit'
    end
  end

  def destroy
  end
  
  def cancelReservation
    @reservation = Reservation.find(params[:id])
    
    if @reservation.update(:updated_at => DateTime.now, :updated_by => current_user.name, :reservation_status_id => 2)
      flash[:success] = t("laluna.messages.updated_success")
      
      redirect_to admin_reservations_path
    else
      flash[:success] = t("laluna.messages.updated_failed")
      render 'edit'
    end
  end
  
  def confirmReservationOK
    @reservation = Reservation.find(params[:id])
    
    if @reservation.update(:updated_at => DateTime.now, :updated_by => current_user.name, :reservation_status_id => 3)
      flash[:success] = t("laluna.messages.updated_success")
      redirect_to admin_reservations_path
    else
      flash[:success] = t("laluna.messages.updated_failed")
      render 'edit'
    end
  end
  
  def confirmReservationNoK
    @reservation = Reservation.find(params[:id])
    
    if @reservation.update(:updated_at => DateTime.now, :updated_by => current_user.name, :reservation_status_id => 4)
      flash[:success] = t("laluna.messages.updated_success")
      redirect_to admin_reservations_path
    else
      flash[:success] = t("laluna.messages.updated_failed")
      render 'edit'
    end
  end
  
  def getListCurrentData
    @lang = Language.find_by("name": params[:locale])
    
    if is_admin?
      @reservations = Reservation.where(delete_flag: 0).all
    else
      @locationOfMine = Location.where(delete_flag: 0 , user_id:current_user.id).all
      @reservations = Reservation.where(delete_flag:0, created_by: current_user.name).or(Reservation.where(delete_flag: 0, location_id: @locationOfMine))
    end
     
     @listData = []
     
     @reservations.each do |f|
      @item = {}
      
      @time = (f.hours < 10 ? "0"+f.hours.to_s : f.hours.to_s) + ":" + (f.minutes < 10 ? "0"+f.minutes.to_s : f.minutes.to_s)
      
      @item["title"] = f.name
      @item["start"] = f.reservation_date.strftime("%Y-%m-%d") + " " + @time + ":00"
      @item["hours"] = f.hours < 10 ? "0"+f.hours.to_s : f.hours.to_s
      @item["minutes"] = f.minutes < 10 ? "0"+f.minutes.to_s : f.minutes.to_s
      @item["resDate"] = f.reservation_date.strftime("%d-%m-%Y")
      @item["adult"] = f.adult.to_s
      @item["child"] = f.child.to_s
      @item["locname"] = LocationTranslation.find_by(location_id:f.location_id, language_id:@lang.id).name
      @item["resuser"] = f.updated_by.to_s
      
      if f.reservation_status_id == 1
        @item["color"] = "#3a87ad"
      elsif f.reservation_status_id == 3
        @item["color"] = "#8cc171"
      elsif f.reservation_status_id == 2
        @item["color"] = "#fa832b"
      else 
        @item ["color"] = "#c0422c"
      end
        
      @listData.push(@item)
     end
     
     send_data @listData.to_json
     #send_data [{"title":"demo", "start":"2017-02-15"}]
  end
  
  private
  
    def reservation_params
      params.require(:reservation).permit(:location_id, :name, :description, :reservation_date, :hours, :minutes, :adult, :child, :created_by, :updated_by, :delete_flag)
    end
    
    def isExistRes? reservation
      @res = Reservation.where(delete_flag: 0,
                        name: reservation.name,
                        adult: reservation.adult,
                        child: reservation.child,
                        reservation_status_id: 1,
                        hours: reservation.hours,
                        minutes: reservation.minutes,
                        location_id: reservation.location_id,
                        reservation_date: reservation.reservation_date
                        )
      if @res.length > 0
        true
      else
        false
      end
    end
    
    def isTimeValid? reservation 
      @resTime = reservation.reservation_date + (reservation.hours*60*60) + (reservation.minutes*60)
      
      @currTime = Time.zone.now
      @checkingTime = @currTime + (2*60*60)
      if @resTime >= @checkingTime
        true
      else
        false
      end
    end
end
