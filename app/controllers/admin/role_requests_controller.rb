class Admin::RoleRequestsController < Admin::ApplicationController
  def new
    @roleRequest = RoleRequest.new
  end

  def create
    @roleRequest = RoleRequest.new(role_request_params)
    @roleRequest.created_by = "Admin"
    @roleRequest.updated_by = "Admin"
    @roleRequest.delete_flag = 0
    
    if @roleRequest.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_role_requests_path
    else
      render 'new'
    end
    
  end

  def edit
  end

  def update
  end

  def index
    @roleRequests = RoleRequest.where(delete_flag: 0).all
  end

  def show
  end

  def destroy
  end
  
  private
    def role_params
      params.require(:role_request).permit(:user_id, :role_id, :from_role, :request_status_id, :reason, :created_by, :updated_by, :delete_flag)
    end
end
