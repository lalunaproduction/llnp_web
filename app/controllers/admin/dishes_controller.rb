class Admin::DishesController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @dish = Dish.new
  end

  def create
    @dish = Dish.new(dish_params)
    @dish.created_by = current_user.name
    @dish.updated_by = current_user.name
    @dish.delete_flag = 0
    
    if @dish.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_dishes_path
    else
      render 'new'
    end
  end

  def index
    @dishes = Dish.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def dish_params
      params.require(:dish).permit(:name, :price, :description, :created_by, :updated_by, :delete_flag)
    end
end
