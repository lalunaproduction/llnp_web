class Admin::MaterialOrdersController < Admin::ApplicationController
  before_filter :verify_logged
  
  def new
    @order = MaterialOrder.new
  end

  def create
    @order = MaterialOrder.new(material_params)
    @order.created_by = current_user.name
    @order.updated_by = current_user.name
    @order.delete_flag = 0
    
    if @order.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_material_orders_path
    else
      render 'new'
    end
  end

  def index
    @orders = MaterialOrder.where(delete_flag: 0).all
  end

  def show
  end

  def edit
    @order = MaterialOrder.find(params[:id])
    @order.order_date = (@order.order_date.to_date).strftime("%d-%m-%Y")
    
  end

  def update
    @order = MaterialOrder.find(params[:id])
    @order.order_location_id = params[:material_order][:order_location_id]
    @order.quantity = params[:material_order][:quantity]
    @order.order_date = (params[:material_order][:order_date].to_date).strftime('%Y-%m-%d')
    @order.description = params[:material_order][:description]
    
    if @order.save
      flash[:success] = 'Cập nhật thành công'
      redirect_to admin_material_orders_path
    else
      render 'edit'
    end
  end

  def destroy
  end
  
  private
    def material_params
      params.require(:material_order).permit(:material_id, :order_location_id, :description, :quantity, :order_date, :created_by, :updated_by, :delete_flag)
    end
end
