class ReservationSerializer < ActiveModel::Serializer
    attributes :id, :name, :reservation_date, :adult, :child, :hours, :minutes
end